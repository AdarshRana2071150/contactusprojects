package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//import org.apache.catalina.User;

import model.User;
import dao.RequestDao;
import dao.UserDao;

@WebServlet("/dashboard")
public class DashboardServlet extends HttpServlet{
	private static final long serialVersionUID = 7240955538526290840L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user = new User();
		user.setId(session.getAttribute("userId") == null ? "0" : (String) session.getAttribute("userId"));
		user.setPassword((String)session.getAttribute("password"));
		UserDao userDao = new UserDao();
			
		try {
			if(userDao.isValidUser(user)) {
				String result = null;
				RequestDao requestDao = new RequestDao();
				result = requestDao.fetchRequests(request.getParameter("isactive"));
				session.setAttribute("result", result);
				response.sendRedirect("dashboard.jsp");
			} else {
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.jsp");
				requestDispatcher.forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("dashboard.jsp");
		RequestDao requestDao = new RequestDao();
		
		try {
			requestDao.doArchive(Integer.parseInt(request.getParameter("id")));
			requestDispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
