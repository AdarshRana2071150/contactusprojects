package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.RequestDao;
import model.Request;

@WebServlet("/contactus")
public class ContactUsServlet extends HttpServlet{ 
	private static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("contactus.jsp");
		requestDispatcher.forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Request req = new Request();
		RequestDao requsetDao = new RequestDao();
		
		req.setFullName(request.getParameter("fullName"));
		req.setEmail(request.getParameter("email"));
		
		try {
			requsetDao.saveRequest(req);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("contactus.jsp");
			requestDispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
}
