package dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Request;

public class RequestDao {
	private String dbUrl = "jdbc:postgresql://localhost:5432/test";
    private String dbUname = "xiomi";
    private String dbPassword = "notebook";
    private String dbDriver = "org.postgresql.Driver";
    
	public void saveRequest(Request request) throws ClassNotFoundException, SQLException   {
	    
		Class.forName(dbDriver);
		Connection connection = DriverManager.getConnection(dbUrl, dbUname, dbPassword);
		String statement = "insert into request (fullName, email, message) values(?, ?, ?)";
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareStatement(statement);
			preparedStatement.setString(1,  request.getFullName());
			preparedStatement.setString(2,  request.getEmail());
			preparedStatement.setString(3,  request.getMessage());
			preparedStatement.executeUpdate();
		} catch(Exception e) {
			e.getStackTrace();
		}
	}
	
	public String fetchRequests(String isactive) throws ClassNotFoundException, SQLException   {
		Class.forName(dbDriver);
		Connection connection = DriverManager.getConnection(dbUrl, dbUname, dbPassword);
		String statement = "select * from request where isactive = ?";
		PreparedStatement preparedStatement = connection.prepareStatement(statement);
		
		preparedStatement.setString(1,isactive);
		ResultSet resultSet = preparedStatement.executeQuery();
		String result = new String();
		
		while(resultSet.next()) {
			result += resultSet.getInt("id")+",";
			result += resultSet.getString("fullName") + ",";
			result += resultSet.getString("email") + ",";
			result += resultSet.getString("message")+"\n";
		}
		return result;
	}
	
	public void doArchive(int id) throws ClassNotFoundException, SQLException  {
		Class.forName(dbDriver);
		Connection connection = DriverManager.getConnection(dbUrl, dbUname, dbPassword);
		String statement = "update request set isActive = 1 where id = ?";
		PreparedStatement preparedStatement;
		
		preparedStatement = connection.prepareStatement(statement);
		preparedStatement.setInt(1, id);
		preparedStatement.executeUpdate();
	}
}
