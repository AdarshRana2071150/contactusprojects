package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.User;

public class UserDao {
	private String dbUrl = "jdbc:postgresql://localhost:5432/test";
    private String dbUname = "xiomi";
    private String dbPassword = "notebook";
    private String dbDriver = "org.postgresql.Driver";
    
	public boolean isValidUser(User user) throws ClassNotFoundException, SQLException  {
		Class.forName(dbDriver);
		Connection connection = DriverManager.getConnection(dbUrl, dbUname, dbPassword);
		PreparedStatement preparedStatement;
		
		preparedStatement = connection.prepareStatement("select * from adminUser where id = ? and  password = ?");
		preparedStatement.setString(1,  user.getId());
		preparedStatement.setString(2,  user.getPassword());
		ResultSet resultSet = preparedStatement.executeQuery();
        return resultSet.next();
	}
}
